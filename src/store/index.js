import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store(options:
  {
  state: {
    games:[
      {name:'Borderlands 3',
price:' 499 ₽',
img: require('@/assets/IMG/Borderlands 3.png')
      },
      {  name:'The Outer Worlds',
        price:' 659 ₽',
        img: require('@/assets/IMG/The Outer Worlds.png')
              },
      { name:'Red Dead Redemption 2',
                price:' 1249 ₽',
               img: require('@/assets/IMG/RDR2.png')
                      },
       { name:'Factorio',
         price:' 659 ₽',
       img: require('@/assets/IMG/factorio.png')
 },
    ],

    search:'',
  },
  getters: {
   getGames: state => state.games,
  getSearch: state => state.search },

  mutations: {
    setGames(state, payload) {
      state.games = payload
    },

    setSearch(state, payload) {
      state.search = payload
    }
  }
})
